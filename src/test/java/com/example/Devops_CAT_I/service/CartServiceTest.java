package com.example.Devops_CAT_I.service;

import com.example.Devops_CAT_I.model.Cart;
import com.example.Devops_CAT_I.model.Item;
import com.example.Devops_CAT_I.repository.CartRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CartServiceTest {

    @Mock
    private CartRepository cartRepositoryMock;

    @InjectMocks
    private CartService cartService;


    @Test
    public void createCart_TestPass(){
        List list = Arrays.asList(new Item(1,"item",1, 10));
        when(cartRepositoryMock.save(ArgumentMatchers.any(Cart.class))).thenReturn(new Cart(1,list,10,4));
        assertEquals(10,cartService.createCart().getTotalPrice());
    }

//    @Test
//    public void removeItemToCartTest() throws Exception {
//        Cart shoppingCart = new Cart(1);    }


}
