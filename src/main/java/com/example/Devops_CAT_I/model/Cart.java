package com.example.Devops_CAT_I.model;

import java.util.List;

public class Cart {

    private int id;
    private List<Item> items;
    private int totalPrice;
    private int numberOfItems;

    public Cart(){
        this.numberOfItems = 0;
        this.totalPrice = 0;
    }

    public Cart(int id, List items, int totalPrice, int numberOfItems) {
        this.id = id;
        this.items = items;
        this.totalPrice = totalPrice;
        this.numberOfItems = numberOfItems;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public void totalPrice(){
        int total = 0;
        for(Item item: this.items) {
            total +=item.getValue();
        }
        this.totalPrice = total;
    }
}
