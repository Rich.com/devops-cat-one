package com.example.Devops_CAT_I;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevopsCatIApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevopsCatIApplication.class, args);
	}

}
