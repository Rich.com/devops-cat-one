package com.example.Devops_CAT_I.service;

import com.example.Devops_CAT_I.model.Cart;
import com.example.Devops_CAT_I.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CartService{

    @Autowired
    private CartRepository cartRepository;
    public List<Cart> getAll() {

        List<Cart> items = cartRepository.findAll();

        return items;
    }

    public Cart createCart(){
        Cart cart= new Cart();
        return  cartRepository.save(cart);
    }

}
