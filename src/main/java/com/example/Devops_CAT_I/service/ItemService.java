package com.example.Devops_CAT_I.service;

import com.example.Devops_CAT_I.model.Item;
import com.example.Devops_CAT_I.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ItemService {

    @Autowired
    ItemRepository itemRepository;

    public Item createItem(int id, String name, int price){
        Item item = new Item(id, name, price);
        return itemRepository.save(item);
    }

    public List<Item> getAll() {

        List<Item> items = itemRepository.findAll();

        for(Item item:items) {
            item.setValue(item.getQuantity()*item.getPrice());
        }
        return items;
    }
}
