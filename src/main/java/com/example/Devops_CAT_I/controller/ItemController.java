package com.example.Devops_CAT_I.controller;

import com.example.Devops_CAT_I.model.Item;
import com.example.Devops_CAT_I.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class ItemController {

    @Autowired
    ItemService itemService;

    @PostMapping("/add-item")
    public Item addItem(@RequestBody Item item){
        return itemService.createItem(item.getId(),item.getName(), item.getPrice());
    }
}
