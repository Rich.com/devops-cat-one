package com.example.Devops_CAT_I.repository;

import com.example.Devops_CAT_I.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Integer> {
}


