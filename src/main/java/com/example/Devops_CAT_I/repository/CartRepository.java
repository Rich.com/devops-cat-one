package com.example.Devops_CAT_I.repository;

import com.example.Devops_CAT_I.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Integer> {
}
